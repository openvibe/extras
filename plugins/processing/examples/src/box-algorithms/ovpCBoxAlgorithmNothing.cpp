#include "ovpCBoxAlgorithmNothing.h"

namespace OpenViBE {
namespace Plugins {
namespace Examples {

bool CBoxAlgorithmNothing::initialize() { return true; }
bool CBoxAlgorithmNothing::uninitialize() { return true; }
bool CBoxAlgorithmNothing::process() { return true; }
}  // namespace Examples
}  // namespace Plugins
}  // namespace OpenViBE
