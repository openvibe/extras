//#include "defines.hpp"

//#include "algorithms/CAlgorithmAddition.hpp"
//#include "box-algorithms/CBoxAlgorithmAdditionTest.hpp"
//#include "box-algorithms/CCrashingBox.hpp"
#include "box-algorithms/CTestCodecToolkit.hpp"

namespace OpenViBE {
namespace Plugins {
namespace Tests {

OVP_Declare_Begin()
	// OVP_Declare_New(CAlgorithmAdditionDesc)
	// OVP_Declare_New(CBoxAlgorithmAdditionTestDesc)
	// OVP_Declare_New(CCrashingBoxDesc)	
	OVP_Declare_New(CTestCodecToolkitDesc)
OVP_Declare_End()

}  // namespace Tests
}  // namespace Plugins
}  // namespace OpenViBE
