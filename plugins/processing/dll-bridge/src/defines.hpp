///-------------------------------------------------------------------------------------------------
/// 
/// \file defines.hpp
/// \copyright Copyright (C) 2022 Inria
///
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published
/// by the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
///
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define Box_DLLBridge									OpenViBE::CIdentifier(0x6F837408, 0x35BA0312)
#define Box_DLLBridgeDesc								OpenViBE::CIdentifier(0x2BB7368F, 0x207D6485)

// Global defines
//---------------------------------------------------------------------------------------------------
#define Box_FlagIsUnstable								OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)
